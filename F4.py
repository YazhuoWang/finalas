import openpyxl
import sqlite3

excel = openpyxl.load_workbook("World_Temperature.xlsx")
ws = excel.create_sheet(title = "Comparison")
sheet = excel.get_sheet_by_name("Comparison")
connection = sqlite3.connect('GlobalLandTemperatures.db')
cursor = connection.cursor()

cursor.execute("""drop table if exists AvState""")
sql_command1 = """
CREATE TABLE AvState
(
date TEXT,
temp REAL,
state TEXT
)"""
cursor.execute(sql_command1)

cursor.execute("""drop table if exists AvAustralia""")
sql_command = """
CREATE TABLE AvAustralia
(
date TEXT,
temp REAL,
country TEXT
);
"""
cursor.execute(sql_command)

sql_command2 = """
SELECT date, AverageTemp, state
FROM GlobalLandTemperaturesByState
WHERE country='Australia'
ORDER BY state, date;
"""
cursor.execute(sql_command2)
result1 = cursor.fetchall()
for i in result1:
    date = i[0]
    temp = i[1]
    state = i[2]
    sql_command = """
    INSERT INTO AvState
    VALUES ('{date}', '{temp}', '{state}');
    """
    cursor.execute(sql_command.format(date=date, temp=temp, state=state))

sql_command3 = """
SELECT SUM(temp) AS sum, COUNT(temp) AS count, state, strftime('%Y', date) AS year
FROM AvState
GROUP BY state, year; 
"""
cursor.execute(sql_command3)

result1 = cursor.fetchall()
syear = list()
stemp = list()
for i in range(len(result1)):
    sheet.cell(row=i+2, column=1).value = result1[i][3]
    sheet.cell(row=i+2, column=2).value = result1[i][0]/result1[i][1]
    sheet.cell(row=i+2, column=3).value = result1[i][2]
    sheet.cell(row=i+2, column=4).value = 'Australia'
    syear.append(result1[i][3])
    stemp.append(result1[i][0]/result1[i][1])
sheet['A1'] = 'Year'
sheet['B1'] = 'AverageTemperature'
sheet['C1'] = 'State'
sheet['D1'] = 'Country'
row1 = sheet.max_row

sql_command4 = """
SELECT date, AverageTemp, country
FROM GlobalLandTemperaturesByCountry
WHERE country='Australia'
ORDER BY date;
"""
cursor.execute(sql_command4)
result2 = cursor.fetchall()
for item in result2:
    date = item[0]
    temp = item[1]
    country = item[2]
    sql_command = """
    INSERT INTO AvAustralia
    VALUES ('{date}', '{temp}', '{country}');
    """
sql_command5 = """
SELECT SUM(temp) AS whole, COUNT(temp) AS count, strftime('%Y', date) AS year, country
FROM AvAustralia
GROUP BY year; 
"""
cursor.execute(sql_command5)
result2 = cursor.fetchall()
ayear = list()
atemp = list()
Max_Row = sheet.max_row
for i in range(len(result2)):
    sheet.cell(row=Max_Row+i+1, column=1).value = result2[i][2]
    sheet.cell(row=Max_Row+i+1, column=2).value = result2[i][0]/result2[i][1]
    sheet.cell(row=Max_Row+i+1, column=3).value = 'NULL'
    sheet.cell(row=Max_Row+i+1, column=4).value = result2[i][3]
    ayear.append(result2[i][2])
    atemp.append(result2[i][0]/result1[i][1])

excel.save("World_Temperature.xlsx")
excel.close()