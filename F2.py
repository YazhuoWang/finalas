import sqlite3

connection = sqlite3.connect("GlobalLandTemperatures.db")
cursor = connection.cursor()

cursor.execute("""drop table if exists South""")
sql_command1 = """
CREATE TABLE South
(
city CHAR(20), 
country CHAR(20),
latitude CHAR(10),
longitude CHAR(10)
)"""
cursor.execute(sql_command1)


sql_command2 = """
SELECT DISTINCT city, country, latitude, longitude
FROM GlobalLandTemperaturesByMajorCity
WHERE latitude LIKE '%S'
ORDER BY country;
"""
cursor.execute(sql_command2)

sql_command3 = """
SELECT DISTINCT city, country, latitude, longitude
FROM GlobalLandTemperaturesByMajorCity
WHERE latitude LIKE '%S'
ORDER BY country;
"""
cursor.execute(sql_command3)

result = cursor.fetchall()
for i in result:
    city = i[0]
    country = i[1]
    latitude = i[2]
    longitude = i[3]
    sql_command4 = """
    INSERT INTO South
    VALUES ('{city}', '{country}', '{latitude}', '{longitude}');
    """
    cursor.execute(sql_command4)


sql_command15 = """
SELECT MAX(AverageTemp), MIN(AverageTemp), AVG(AverageTemp)
FROM GlobalLandTemperaturesByState
WHERE date LIKE '2000%' and state = 'Queensland';
"""

sql_command6 = """
SELECT MAX(AverageTemp), MIN(AverageTemp), AVG(AverageTemp)
FROM GlobalLandTemperaturesByState
WHERE date LIKE '2000%';
"""
cursor.execute(sql_command6)

result = cursor.fetchall()

for i in result:
    print(i)