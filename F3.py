import openpyxl
import sqlite3

excel = openpyxl.load_workbook("World_Temperature.xlsx")
ws = excel.create_sheet(title = "TemperatureByCity")
sheet = excel.get_sheet_by_name("TemperatureByCity")
print("1")
connection = sqlite3.connect("GlobalLandTemperatures.db")
cursor = connection.cursor()

cursor.execute("""drop table if exists AvaTempCity""")
sql_command = """
CREATE TABLE AvaTempCity
(
date DATA,
temp FLOAT,
city CHAR(20)
)"""
cursor.execute(sql_command)
print("1")
sql_command1 = """
SELECT date, AverageTemp, city
FROM  GlobalLandTemperaturesByMajorCity
WHERE country='China'
ORDER BY city, date;
"""
cursor.execute(sql_command1)
print("2")
result = cursor.fetchall()
for i in result:
    date = i[0]
    temp = i[1]
    city = i[2]
    sql_command = """
    INSERT INTO AvaTempCity
    VALUES("{date}", "{temp}", "{city}")"""
    cursor.execute(sql_command.format(date=date, temp=temp, city=city))

sql_command = """
SELECT SUM(temp) AS sum, COUNT(temp) AS count, city, strftime('%Y', date) AS year
FROM AvaTempCity
GROUP BY city, year; 
"""
cursor.execute(sql_command)
result = cursor.fetchall()
for i in range(len(result)):
    sheet.cell(row = i + 2, column = 1).value = result[i][0] / result[i][1]
    sheet.cell(row = i + 2, column = 2).value = result[i][2]
    sheet.cell(row = i + 2, column = 3).value = result[i][3]


sheet['A1'] = "AverageTemperature"
sheet['B1'] = "City"
sheet['C1'] = "Year"


excel.save("World_Temperature.xlsx")
excel.close()
